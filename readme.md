# VSMonokai #

VSMonokai is a color theme settings which provides monokai like color schema for people who use Visual Studio 2012.
<br>
<br>
## Installation ##

1. Backup your settings before installation.
2. Change you color theme of Visual Studio to **Dark**. (This step is optional)
3. Import the settings by below steps.
	
	TOOLS → Import and Export Settings → Import Selected Enviroment Settings

